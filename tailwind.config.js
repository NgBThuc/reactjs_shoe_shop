/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      "wild-sand": "#f6f6f6",
      nobel: "#b3b3b3",
      "cod-gray": "#0d0d0d",
      "clam-shell": "#d1b9b2",
      "shuttle-gray": "#5f6061",
      masala: "#433937",
      cerulean: "#11a5cd",
      brown: "#C85D46",
    },
    fontFamily: {
      sans: ["Poppins", "sans-serif"],
    },
  },
  plugins: [],
};
