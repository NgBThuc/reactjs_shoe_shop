import React, { Component } from "react";

export default class ShoeDetailPopup extends Component {
  render() {
    let isVisible = this.props.isShoeDetailVisible;
    let shoeDetail = this.props.shoeDetail;

    return (
      <div
        className={`fixed p-8 bottom-0 left-0 ${
          isVisible ? "" : "-translate-x-full"
        } transition-all border-2 border-nobel bg-wild-sand rounded-tr-lg rounded-br-lg h-screen w-[40vw] shadow-xl`}
      >
        <div
          onClick={this.props.handleCloseShoeDetail}
          className="text-right group"
        >
          <i className="text-3xl fa fa-times-circle transition-colors group-hover:text-brown"></i>
        </div>

        <h3 className="text-3xl font-bold text-center text-brown mb-8">
          {shoeDetail.name}
        </h3>

        <div className="mb-8">
          <h4 className="text-xl font-bold text-shuttle-gray mb-4">
            Description
          </h4>
          <p className="text-lg mb-3">{shoeDetail.description}</p>
          <p className="text-lg">{shoeDetail.shortDescription}</p>
        </div>

        <div className="mb-8">
          <h4 className="text-xl font-bold text-shuttle-gray mb-3">Price</h4>
          <p className="text-3xl font-bold text-cerulean">
            {shoeDetail.price}$
          </p>
        </div>

        <img
          className="w-full h-[400px] object-cover border-2 border-brown shadow-lg rounded-lg"
          src={shoeDetail.image}
          alt=""
        />
      </div>
    );
  }
}
