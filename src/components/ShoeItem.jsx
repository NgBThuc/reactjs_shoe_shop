import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    let shoe = this.props.shoe;

    return (
      <div className="p-5 border-2 border-nobel rounded-lg">
        <h3 className="text-xl font-bold">{shoe.name}</h3>

        <img
          className="w-full h-72 object-cover py-4 rounded-lg"
          src={shoe.image}
          alt={shoe.alias}
        />

        <div className="flex justify-between items-center">
          <div className="flex flex-col gap-1">
            <span className="text-shuttle-gray">Price</span>
            <span className="text-2xl font-bold">{shoe.price}$</span>
          </div>

          <div className="flex items-center gap-2">
            <button
              onClick={() => {
                this.props.handleAddToCart(shoe);
              }}
              className="py-2 px-3 border-2 border-cod-gray bg-cod-gray text-wild-sand rounded-full transition-all hover:-translate-y-1 hover:shadow-md"
            >
              Add to cart
            </button>
            <button
              onClick={() => this.props.handleShowShoeDetail(shoe)}
              className="py-2 px-3 border-2 border-brown bg-brown text-wild-sand rounded-full transition-all hover:-translate-y-1 hover:shadow-md"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
