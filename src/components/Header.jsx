import React, { Component } from "react";

export default class Header extends Component {
  render() {
    let cart = this.props.cart;

    const showCartItemCount = () => {
      if (cart.length === 0) {
        return null;
      } else if (cart.length > 99) {
        <span className="grid place-content-center text-xs text-wild-sand h-6 w-6 bg-brown absolute -right-3 -top-3 rounded-full">
          99+
        </span>;
      } else {
        return (
          <span className="grid place-content-center text-xs text-wild-sand h-6 w-6 bg-brown absolute -right-3 -top-3 rounded-full">
            {cart.length}
          </span>
        );
      }
    };

    return (
      <div className="flex justify-between items-center font-sans h-12">
        <span className="text-2xl text-shuttle-gray font-medium transition-colors cursor-pointer hover:text-brown">
          Shoe.
        </span>

        <div className="relative border-2 rounded-lg w-2/5 border-nobel transition-colors hover:border-brown">
          <label
            className="absolute top-1/2 left-2 -translate-y-1/2 cursor-pointer group"
            htmlFor="headerSearch"
          >
            <i className="text-shuttle-gray fa fa-search group-hover:text-brown transition-colors"></i>
          </label>
          <input
            className="rounded-lg py-2 pl-8 pr-2 w-full focus:outline-none placeholder:font-sans"
            type="text"
            id="headerSearch"
            placeholder="Search shoes here"
          />
        </div>

        <div className="flex justify-between gap-3">
          <i
            onClick={this.props.handleShowCart}
            className="relative text-base text-shuttle-gray w-10 h-10 border-2 border-nobel rounded-full grid place-content-center transition-colors fa fa-shopping-bag cursor-pointer hover:text-brown hover:border-brown"
          >
            {showCartItemCount()}
          </i>
          <i className="text-base text-shuttle-gray w-10 h-10 border-2 border-nobel rounded-full grid place-content-center fa fa-user transition-colors hover:text-brown hover:border-brown"></i>
        </div>
      </div>
    );
  }
}
