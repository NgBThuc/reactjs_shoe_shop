import React, { Component } from "react";
import ShoeItem from "./ShoeItem";

export default class ShoesList extends Component {
  render() {
    let shoesArray = this.props.shoesArray;

    return (
      <div className="py-10">
        <h1 className="text-2xl font-bold">Shoes Products</h1>

        <div className="py-5 grid grid-cols-3 gap-4">
          {shoesArray.map((shoe) => (
            <ShoeItem
              handleAddToCart={this.props.handleAddToCart}
              handleShowShoeDetail={this.props.handleShowShoeDetail}
              key={shoe.id}
              shoe={shoe}
            />
          ))}
        </div>
      </div>
    );
  }
}
