import React, { Component } from "react";

export default class CartItem extends Component {
  render() {
    let cartItem = this.props.cartItem;

    return (
      <div className="flex items-center justify-between border-2 border-nobel p-4 rounded-lg">
        <div className="flex items-center gap-4">
          <img className="w-20 h-20 object-cover" src={cartItem.image} alt="" />
          <div>
            <p className="text-xl font-semibold mb-2">{cartItem.name}</p>
            <div className="flex items-center gap-2 pt-1 text-xl">
              <i
                onClick={() => this.props.handleDecreaseQuantity(cartItem.id)}
                className="fa fa-minus-circle transition-colors cursor-pointer hover:text-brown"
              ></i>
              <p>{cartItem.quantity}</p>
              <i
                onClick={() => this.props.handleIncreaseQuantity(cartItem.id)}
                className="fa fa-plus-circle transition-colors cursor-pointer hover:text-brown"
              ></i>
            </div>
          </div>
        </div>
        <div className="flex items-center gap-4">
          <p className="text-xl font-bold text-brown">{cartItem.price}$</p>
          <i
            onClick={() => this.props.handleDeleteCartItem(cartItem.id)}
            className="text-2xl fa fa-trash-alt transition-colors hover:text-cerulean cursor-pointer"
          ></i>
        </div>
      </div>
    );
  }
}
