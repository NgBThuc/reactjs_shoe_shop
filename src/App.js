import React, { Component } from "react";
import "./App.css";
import Cart from "./components/Cart";
import Header from "./components/Header";
import ShoeDetailPopup from "./components/ShoeDetailPopup";
import ShoesList from "./components/ShoesList";
import { dataArray } from "./data/shoesData";

export default class App extends Component {
  state = {
    shoesArray: dataArray,
    shoeDetail: {},
    cart: [],
    isShoeDetailVisible: false,
    isCartVisible: false,
  };

  handleAddToCart = (selectedShoe) => {
    let cart = [...this.state.cart];
    let cartItem = { ...selectedShoe, quantity: 1 };
    let index = cart.findIndex((cartItem) => cartItem.id === selectedShoe.id);

    if (index === -1) {
      cart.push(cartItem);
    } else {
      cart[index].quantity++;
    }

    this.setState({
      cart,
    });
  };

  handleShowShoeDetail = (selectedShoe) => {
    this.setState({
      shoeDetail: selectedShoe,
      isShoeDetailVisible: true,
    });
  };

  handleCloseShoeDetail = () => {
    this.setState({
      isShoeDetailVisible: false,
    });
  };

  handleShowCart = () => {
    this.setState({
      isCartVisible: true,
    });
  };

  handleHideCart = () => {
    this.setState({
      isCartVisible: false,
    });
  };

  handleIncreaseQuantity = (cartItemId) => {
    let cart = [...this.state.cart];
    let index = cart.findIndex((cartItem) => cartItem.id === cartItemId);
    cart[index].quantity++;
    this.setState({
      cart,
    });
  };

  handleDecreaseQuantity = (cartItemId) => {
    let cart = [...this.state.cart];
    let index = cart.findIndex((cartItem) => cartItem.id === cartItemId);
    if (cart[index].quantity === 1) {
      return;
    }
    cart[index].quantity--;
    this.setState({
      cart,
    });
  };

  handleDeleteCartItem = (cartItemId) => {
    let cart = [...this.state.cart];
    cart = cart.filter((cartItem) => cartItem.id !== cartItemId);
    this.setState({
      cart,
    });
  };

  render() {
    return (
      <div className="container mx-auto py-5 relative">
        <Header cart={this.state.cart} handleShowCart={this.handleShowCart} />
        <ShoesList
          shoesArray={this.state.shoesArray}
          handleShowShoeDetail={this.handleShowShoeDetail}
          handleAddToCart={this.handleAddToCart}
        />
        <ShoeDetailPopup
          handleCloseShoeDetail={this.handleCloseShoeDetail}
          isShoeDetailVisible={this.state.isShoeDetailVisible}
          shoeDetail={this.state.shoeDetail}
        />
        <Cart
          cart={this.state.cart}
          handleHideCart={this.handleHideCart}
          handleIncreaseQuantity={this.handleIncreaseQuantity}
          handleDecreaseQuantity={this.handleDecreaseQuantity}
          handleDeleteCartItem={this.handleDeleteCartItem}
          isCartVisible={this.state.isCartVisible}
        />
      </div>
    );
  }
}
